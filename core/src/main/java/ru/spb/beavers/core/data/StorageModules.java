package ru.spb.beavers.core.data;

import ru.spb.beavers.modules.ITaskModule;

import java.io.File;
import java.net.URL;
import java.util.LinkedList;

/**
 * Хранилище модулей
 */
public class StorageModules {

    private static final LinkedList<ITaskModule> MODULES = new LinkedList<>();
    private static ITaskModule activeModule;

    static {
        try {
            String packageName = ITaskModule.class.getPackage().getName().replace(".", "/");
            URL resource = ClassLoader.getSystemClassLoader().getResource(packageName);

            assert resource != null;
            File modulesDir = new File(resource.toURI());
            File[] moduleClasses = modulesDir.listFiles();

            assert moduleClasses != null;
            for (File moduleClass : moduleClasses) {
                if (moduleClass.isDirectory()) {
                    continue;
                }
                String className = moduleClass.getName().replace(".class", "");
                Class<?> module = Class.forName("ru.spb.beavers.modules.".concat(className));
                if (!module.isInterface() && ITaskModule.class.isAssignableFrom(module)) {
                    MODULES.add((ITaskModule) module.newInstance());
                }
            }
            if (MODULES.isEmpty()) {
                throw new RuntimeException("Не найдены модули для загрузки!");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static LinkedList<ITaskModule> getModules() {
        return MODULES;
    }

    public static void setActiveModule(ITaskModule newActiveModule) {
        activeModule = newActiveModule ;
    }

    public static ITaskModule getActiveModule() {
        return activeModule;
    }
}
