package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.data.StorageModules;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * View входных параметров модуля
 */
public class InputView extends JPanel {

    private final JPanel inputPanel;
    private final JButton btnGoTheory;
    private final JButton btnGoExample;
    private final JButton btnLoad;
    private final JButton btnSave;
    private final JButton btnDefault;

    public InputView() {
        super(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.weighty = 0.1;
        constraints.gridwidth = 5;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(new JLabel(), constraints);

        constraints.fill = GridBagConstraints.NONE;
        btnGoTheory = new JButton("<");
        btnGoTheory.setToolTipText("Назад к теоретическому решению");
        constraints.insets = new Insets(5, 5, 0, 5);
        constraints.gridwidth = 1;
        constraints.weightx = 0.0;
        constraints.ipady = 22;
        constraints.gridy = 1;
        this.add(btnGoTheory, constraints);

        btnGoExample = new JButton(">");
        btnGoExample.setToolTipText("Теоретическое решение задачи");
        constraints.gridx = 4;
        this.add(btnGoExample, constraints);

        inputPanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(inputPanel);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = new Insets(1, 5, 0, 5);
        constraints.gridwidth = 3;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.gridx = 1;
        this.add(scrollPane, constraints);

        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.fill = GridBagConstraints.NONE;
        constraints.insets = new Insets(0, 5, 0, 5);
        constraints.gridwidth = 1;
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
        constraints.ipadx = -10;
        constraints.ipady = -1;
        constraints.gridy = 2;
        this.add(new JLabel(), constraints);


        btnSave = new JButton();
        btnSave.setToolTipText("Сохранить исходные данные");
        btnSave.setIcon(loadIcon("save.png"));
        this.add(btnSave, constraints);

        btnLoad = new JButton();
        btnLoad.setToolTipText("Загрузить исходные данные");
        btnLoad.setIcon(loadIcon("load.png"));
        this.add(btnLoad, constraints);

        btnDefault = new JButton();
        btnDefault.setToolTipText("Задать значения по умолчанию");
        btnDefault.setIcon(loadIcon("default.png"));
        this.add(btnDefault, constraints);

        this.add(new JLabel(), constraints);

        initListeners();
    }

    private ImageIcon loadIcon(String fileName) {
        try {
            return new ImageIcon(ImageIO.read(getClass().getResource(fileName)));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void initListeners() {
        btnGoTheory.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GUIManager.setActiveView(GUIManager.getTheoryView());
            }
        });

        btnGoExample.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ExampleView exampleView = GUIManager.getExampleView();
                StorageModules.getActiveModule().initExamplePanel(exampleView.getExamplePanel());
                GUIManager.setActiveView(exampleView);
            }
        });
    }

    public void initSaveButtonListener(final ActionListener listener) {
        for (ActionListener actionListener : btnSave.getActionListeners()) {
            btnSave.removeActionListener(actionListener);
        }
        btnSave.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    listener.actionPerformed(event);
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(InputView.this, "Не удалось сохранить",
                            "Ошибка", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    public void initLoadButtonListener(final ActionListener listener) {
        for (ActionListener actionListener : btnLoad.getActionListeners()) {
            btnLoad.removeActionListener(actionListener);
        }
        btnLoad.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    listener.actionPerformed(event);
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(InputView.this, "Не удалось загрузить",
                            "Ошибка", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    public void initDefaultButtonListener(final ActionListener listener) {
        for (ActionListener actionListener : btnDefault.getActionListeners()) {
            btnDefault.removeActionListener(actionListener);
        }
        btnDefault.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    listener.actionPerformed(event);
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(InputView.this, "Не удалось загрузить",
                            "Ошибка", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    public JPanel getInputPanel() {
        return inputPanel;
    }
}
